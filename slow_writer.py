#!/usr/bin/env python3
# Author: Stefan Schindler <dns2utf8@estada.ch>
# LICENCE: AGPL or proprietary

import os.path
import sys
import time

FLUSH_EVERY_N = 10

# print ('Number of arguments:', len(sys.argv), 'arguments.')
# print ('Argument List:', str(sys.argv))
if len(sys.argv) != 2:
    sys.exit("invalid arguments")

target_dir = sys.argv[1]

if not os.path.isdir(target_dir):
    sys.exit("target_dir is not a directory")

target_dir = os.path.abspath(target_dir) + '/'

print('target_dir:', target_dir)
print('PID:', os.getpid())
print('')
time.sleep(2.5)

with open(target_dir + 'slow_writer_test_file.txt', 'w') as f:
    for i in range(2048):
        print('\rwrite', i, end='')
        f.write('{}\n'.format(i))
        if FLUSH_EVERY_N > 0:
            if i % FLUSH_EVERY_N == 0:
                f.flush()
        time.sleep(0.1)

