# xfs_fsfreeze_test

A quick test to see if [XFS Kernel Bug 205833](https://bugzilla.kernel.org/show_bug.cgi?id=205833) still affects your system.

By Stefan Schindler

## Setup

Setup loop back device and a small partition with xfs

```
make setup lo_list
```

## Teardown

Cleanup loop back device

```
make teardown
```

## Testing

Open two terminals in the first one run:

```
make slow_writer
```

Then in the second terminal (as many times as you like):

```
make freeze test_tail
```

Check the stack of the program by using the PID the skript prints at the beginning:

```
sudo cat /proc/${PID}/stack 
```

The `test_tail` should always return, if it hangs you are affected.

### unfreeze

To continue
```
make unfreeze
```
