MOUNT_POINT:="/tmp/xfs_freeze"


lo_list:
	sudo losetup -al

lo_detatch:
	sudo losetup --detach-all


setup:
	# Create just over 300MB partition, which is the minimum
	dd if=/dev/zero bs=4M count=80 of=./a.img
	sudo losetup --sector-size 4096 loop0 a.img

	sudo mkfs.xfs -L xfs_freeze /dev/loop0
	make mount
	make fix_permissions

teardown: umount lo_detatch
	rmdir ${MOUNT_POINT}
	rm ./a.img
	sync

mount:
	mkdir -p ${MOUNT_POINT}
	sudo mount /dev/loop0 ${MOUNT_POINT}

umount:
	sudo umount ${MOUNT_POINT}

fix_permissions:
	sudo chown -R $$(id -u):$$(id -g) ${MOUNT_POINT}/

slow_writer:
	./slow_writer.py ${MOUNT_POINT}

freeze:
	sudo fsfreeze --freeze ${MOUNT_POINT}

unfreeze:
	sudo fsfreeze --unfreeze ${MOUNT_POINT}

test_tail:
	tail ${MOUNT_POINT}/slow_writer_test_file.txt
